# hdctools: Chrome OS Hardware Debug & Control Tools

This repository contains source code and documentation for the Servo debug
boards.

[TOC]

## Servo

*   [Servo: Debug Board](./docs/servo.md)
    *   [Servo Micro](./docs/servo_micro.md)
    *   [Servo v4](./docs/servo_v4.md)

## servod

*   [`servod`: Daemon for Servo](./docs/servod.md)
*   [`servod` FAQ](./docs/servod_faq.md)

## Power Measurement

*   [Power Measurement](./docs/power_measurement.md)
*   [Sweetberry Power Monitoring Board](./docs/sweetberry.md)
*   [INA: Instrumentation Amplifier](./docs/ina.md)

## Resources

*   [File a Bug](https://bugs.chromium.org/p/chromium/issues/entry?components=Tools%3EChromeOSDebugBoards)
*   [Contact](https://chromium.googlesource.com/chromiumos/docs/+/master/contact.md)
