#!/bin/bash -eu
# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Convenient wrapper to run presubmit checks in parallel without interleaving
# output/etc.

if [[ $# -eq 0 ]]; then
  echo "No files were given to check the style of. Exiting." >&2
  exit
fi

mydir="$(dirname "$(readlink -m "$0")")"
pydir="${mydir}/.."

if [[ -z "${PYTHONPATH:-}" ]]; then
  export PYTHONPATH="${pydir}"
else
  export PYTHONPATH="${pydir}:${PYTHONPATH}"
fi

tempfiles=()
rm_tempfiles() {
  rm -f "${tempfiles[@]}"
}

trap rm_tempfiles EXIT

child_pids=()
spawn_child() {
  local tempfile
  tempfile="$(mktemp)"
  tempfiles+=( "${tempfile}" )
  "$@" >"${tempfile}" 2>&1 &
  child_pids+=( "$!" )
}


# only lint existing files
files_exist=false
declare -a to_lint
for file; do
  if [[ -f "${file}" ]]; then
    files_exist=true
    to_lint+=("${file}")
  fi
done

# We have a few things to do in parallel here. To avoid interleaving their
# output, we pipe them all to tempfiles, then cat those tempfiles.
if "${files_exist}"; then
  spawn_child "${mydir}/check-lint" "${to_lint[@]}"
  spawn_child "${mydir}/check-format" "${to_lint[@]}"
  spawn_child "${mydir}/../run_tests_for.py" "${to_lint[@]}"
fi

success=true
for i in "${!child_pids[@]}"; do
  wait "${child_pids[$i]}" || success=false
  cat "${tempfiles[$i]}"
done

"${success}"
