Opcions per a desenvolupadors
Mostra la informació de depuració
Activa la verificació del SO
Apaga
Idioma
Inicia des de la xarxa
Inicia Legacy BIOS
Inicia des de l'USB
Inicieu el sistema des d'una unitat USB o des d'una targeta SD
Inicia des del disc intern
Cancel·la
Confirma l'activació de la verificació del SO
Desactiva la verificació del SO
Confirma la desactivació de la verificació del SO
Utilitzeu els botons de volum per desplaçar-vos amunt o avall
i el botó d'engegada per seleccionar una opció.
Si desactiveu la funció Verificació del SO, el sistema serà INSEGUR.
Seleccioneu "Cancel·la" per mantenir la protecció.
La funció Verificació del SO està desactivada. El sistema és INSEGUR.
Seleccioneu "Activa la verificació del SO" per recuperar la protecció.
Seleccioneu "Confirma l'activació de la verificació del SO" per protegir el sistema.
