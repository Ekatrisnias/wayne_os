Chrome OS no está instalado o está dañado.
Inserta una tarjeta SD o una unidad USB de recuperación.
Inserta una unidad USB de recuperación.
Inserta una unidad USB o una tarjeta SD de recuperación. (Nota: El puerto USB azul NO se puede utilizar para recuperar el sistema).
Inserta una unidad USB de recuperación en uno de los cuatro puertos situados en la PARTE POSTERIOR del dispositivo.
Chrome OS no aparece en el dispositivo insertado.
La función Verificación de SO está DESACTIVADA.
Pulsa la BARRA ESPACIADORA para volver a habilitar la función.
Pulsa INTRO para confirmar que quieres activar la función Verificación de SO.
Se reiniciará el sistema y se borrarán los datos locales.
Para volver atrás, pulsa ESC.
La función Verificación de SO está ACTIVADA.
Para DESACTIVAR la función Verificación de SO, pulsa INTRO.
Si necesitas ayuda, accede a la página https://google.com/chromeos/recovery
Código de error
Retira todos los dispositivos externos conectados para iniciar la recuperación.
Modelo 60061e
Para desactivar la función Verificación de SO, pulsa el botón RECUPERACIÓN.
La fuente de alimentación conectada no tiene suficiente potencia para hacer funcionar este dispositivo.
Chrome OS se cerrará.
Utiliza el adaptador correcto y vuelve a intentarlo.
Retira todos los dispositivos conectados e inicia la recuperación.
Pulsa una tecla numérica para seleccionar un bootloader alternativo:
Pulsa el BOTÓN DE ENCENDIDO para realizar un diagnóstico.
Para desactivar la función Verificación de SO, pulsa el BOTÓN DE ENCENDIDO.
