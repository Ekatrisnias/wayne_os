## Wayne OS
The Wayne OS is based on Chromium OS.

The goal is to upgrade compatibility for general amd64(x86_64) PC.

You can download complete Wayne OS binary at http://www.wayne-inc.com.



## How to install Wayne OS on your USB

1. Download the Installer on http://www.wayne-inc.com.
2. Prepare at least 8GB of USB. If it is not empty, format it first.
3. Plug the prepared USB on your PC.
4. Run the Installer.
5. Follow the description on the installer.



## How to boot Wayne OS installed on your USB

1. Before turning on your PC, Plug your USB on your PC.

2. Turn on your PC and enter into BIOS.  
   The shortcut for entering into BIOS is depending on the brand or manufacturer.  
   If you don't know what the shortcut is, Search for the internet.

    Brand
        
        SAMSUNG : F2 or ESC
        
        LG : F2 or F12
        
        HP : F10 or ESC
        
        DELL : F2 or F12
        
        MSI : F1 or F12
        
        ASUS : F2 or DEL or ESC  		  

   
    Manufacturer(assembled PC)
        
        GIGABYTE : F12
        
        ASRock : F11
        
        Intel : F10
        
        Lenovo : F1 or F2
        
        MSI : DEL


3. Configure booting priority. Set the USB plugged on your PC as the first priority.

4. Save Configuration and Exit.

5. Wayne OS will be booting on your PC.



## License

Refer to [LICENSE]( https://gitlab.com/wayne-inc/wayne_os/blob/master/LICENSE ).



## Contributing

To get more users' hardware information for improving compatibility with general amd64(x86_64) PC, we are waiting for your contributing.  

Refer to [CONTRIBUTING]( https://gitlab.com/wayne-inc/wayne_os/blob/master/CONTRIBUTING.md ).



## Wayne OS Hardware Compatibility 

We arrange the table for collecting hardware specification which is compatible with Wayne OS.

Please fill out the table in [here](https://gitlab.com/wayne-inc/wayne_os/blob/master/Wayne-OS-HW-Compatibility.md) with hardware specification which booting Wayne OS is successful on.

Add a row for filling out the table with hardware specification.

If you feel that some hardware need to be added more for checking compatibility, Please Add a column for that.